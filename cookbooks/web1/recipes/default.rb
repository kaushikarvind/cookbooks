#
# Cookbook:: web1
# Recipe:: default
#
# Copyright:: 2020, The Authors, All Rights Reserved.

package "httpd"

service "httpd" do
	action :start
end

file "/var/www/html/index.html" do
    content "This is coming from an automated CI/CD pipeline setup"
end

